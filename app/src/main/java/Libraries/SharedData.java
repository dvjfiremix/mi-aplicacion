package Libraries;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import static Libraries.Globals.SHARED_APP;

public class SharedData extends AppCompatActivity {
    private static SharedData MyShared = null;
    private SharedPreferences shareds;

    private SharedData(Context context) {
        shareds = context.getSharedPreferences(SHARED_APP, Context.MODE_PRIVATE);
    }

    public static SharedData getInstance(Context context) {
        if (context != null) {
            MyShared = new SharedData(context);
        }
        return MyShared;
    }

    public boolean SaveData(String key, String value, boolean estado, int numero, int opcion) {
        try {
            SharedPreferences.Editor editor = shareds.edit();
            switch (opcion) {
                case 0:
                    editor.putString(key, value);
                    break;

                case 1:
                    editor.putBoolean(key, estado);
                    break;

                case 2:
                    editor.putInt(key, numero);
                    break;
            }
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //Obtiene valores de la preferencia de tipo String
    public String getStringData(String key) {
        String data;
        try {
            data = shareds.getString(key, "");
        } catch (Exception e) {
            data = "";
        }
        return data;
    }
}
