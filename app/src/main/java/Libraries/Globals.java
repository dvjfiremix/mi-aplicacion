package Libraries;

public class Globals {
    public static String SHARED_APP = "MYAPPLICATION";
    public static String MY_JSON = "MyJson";
    public static String JSON_FILES = "MyFiles";
    public static String FOTOS = "Imagenes";
    public static String USUARIO = "MiUsuario";

    public static String getUSUARIO() {
        return USUARIO;
    }

    public static void setUSUARIO(String USUARIO) {
        Globals.USUARIO = USUARIO;
    }
}
