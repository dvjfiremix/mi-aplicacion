package Libraries;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.merlino.miaplicacion.R;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

import static Libraries.Globals.FOTOS;
import static androidx.core.content.FileProvider.getUriForFile;

public class Utils extends AppCompatActivity {
    public static Utils utils = null;
    private Context ctx;

    private Utils(Context context) {
        this.ctx = context;
    }

    public static Utils getInstance(Context context) {
        if (context != null) {
            utils = new Utils(context);
        }
        return utils;
    }

    public void showToast(String text) {
        Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
    }

    public void LoadImage(String imagen, CircleImageView foto) {
        try {
            Glide.with(ctx)
                    .load(imagen)
                    .placeholder(R.drawable.ic_launcher_background)
                    .fitCenter()
                    .into(foto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean CreateFolderInternalApp() {
        try {
            File path = new File(ctx.getExternalCacheDir(), FOTOS);
            if (!path.exists()) {
                path.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //Obtiene la imagen tomada por la cámara para usar cómo foto
    public Uri getImageFromPath(String fileName) {
        File path = new File(ctx.getExternalCacheDir(), FOTOS);
        Uri imagens;
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            imagens = getUriForFile(ctx, "com.merlino.miaplicacion.fileprovider", image);
        } else {
            imagens = Uri.fromFile(image);
        }
        return imagens;
    }

    public void clearImagesFromPath(String FileImagen) {
        File path = new File(ctx.getExternalCacheDir(), FOTOS);
        if (!path.exists()) path.mkdirs();
        File image = new File(path, FileImagen);
        if (image.exists()) {
            image.delete();
        }
    }
}
