package com.merlino.miaplicacion.Login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.merlino.miaplicacion.Home.HomeModel;
import com.merlino.miaplicacion.R;
import com.merlino.miaplicacion.Register.RegisterModel;

import org.json.JSONArray;
import org.json.JSONObject;

import Libraries.SharedData;
import Libraries.Utils;

import static Libraries.Globals.MY_JSON;
import static Libraries.Globals.setUSUARIO;

public class LoginModel extends LoginView implements View.OnClickListener {
    Gson gson = new Gson();
    private Utils utils;
    private SharedData sharedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        utils = Utils.getInstance(this);
        sharedData = SharedData.getInstance(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.Login) {
            if (ValidateData()) {
                String Usuario = username.getText().toString().trim();
                String Password = password.getText().toString().trim();
                ReadUser(Usuario, Password);
            }
        }
        if (id == R.id.Register) {
            Intent n = new Intent(getApplicationContext(), RegisterModel.class);
            startActivity(n);
            finish();
        }
    }

    private boolean ValidateData() {
        try {
            String Usuario = username.getText().toString().trim();
            String Password = password.getText().toString().trim();
            if (Usuario.equals("") && Password.equals("")) {
                utils.showToast("Debe ingresar el usuario y la contraseña");
                return false;
            } else if (Usuario.equals("")) {
                utils.showToast("Debe ingresar el usuario");
                return false;
            } else if (Password.equals("")) {
                utils.showToast("Debe ingresar la contraseña");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean ReadUser(String usuario, String Password) {
        try {
            String info = sharedData.getStringData(MY_JSON);
            if (info.equals("")) {
                utils.showToast("Registrar nuevo usuario");
                Intent n = new Intent(getApplicationContext(), RegisterModel.class);
                startActivity(n);
                finish();
                return false;
            } else {
                if (!gson.toJson(info).equals("[]")) {
                    JSONArray jArray = new JSONArray(info);
                    boolean encontrado = false;
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        if (json_data.getString("Usuario").equals(usuario) && json_data.getString("Password").equals(Password)) {
                            encontrado = true;
                        }
                    }
                    if (!encontrado) {
                        utils.showToast("El usuario " + usuario + " no existe, por favor verifique los datos o registre uno nuevo");
                        return false;
                    } else {
                        utils.showToast("Bienvenido de nuevo: " + usuario);
                        setUSUARIO(usuario);
                        Intent n = new Intent(getApplicationContext(), HomeModel.class);
                        startActivity(n);
                        finish();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


}
