package com.merlino.miaplicacion.Login;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.merlino.miaplicacion.BaseActivity;
import com.merlino.miaplicacion.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginView extends BaseActivity {

    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.Login)
    Button login;
    @BindView(R.id.Register)
    Button register;

    public LoginView() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
    }

}
