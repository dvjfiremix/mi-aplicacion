package com.merlino.miaplicacion.Register;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.merlino.miaplicacion.BaseActivity;
import com.merlino.miaplicacion.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterView extends BaseActivity {

    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm)
    EditText confirm;
    @BindView(R.id.Register)
    Button Register;
    @BindView(R.id.Back)
    Button Back;

    public RegisterView() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        ButterKnife.bind(this);
    }
}
