package com.merlino.miaplicacion.Register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.merlino.miaplicacion.Login.LoginModel;
import com.merlino.miaplicacion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import Libraries.SharedData;
import Libraries.Utils;

import static Libraries.Globals.MY_JSON;

public class RegisterModel extends RegisterView implements View.OnClickListener {
    JSONArray jArray = new JSONArray();
    Gson gson = new Gson();
    private Utils utils;
    private SharedData sharedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Register.setOnClickListener(this);
        Back.setOnClickListener(this);
        utils = Utils.getInstance(this);
        sharedData = SharedData.getInstance(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.Register) {
            if (ValidateData()) {
                String Usuario = username.getText().toString().trim();
                String Password = password.getText().toString().trim();
                if (ReadUser(Usuario, Password)) {
                    utils.showToast("Información guardada correctamente");
                    ClearData();
                    Intent n = new Intent(getApplicationContext(), LoginModel.class);
                    startActivity(n);
                    finish();
                }
            }
        }
        if (id == R.id.Back) {
            Intent n = new Intent(getApplicationContext(), LoginModel.class);
            startActivity(n);
            finish();
        }
    }

    private String JsonUser(String usuario, String contrasena) {
        String jsons = null;
        try {
            JSONObject users = new JSONObject();
            users.put("Usuario", usuario);
            users.put("Password", contrasena);
            jArray.put(users);
            jsons = jArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsons;
    }

    private boolean ValidateData() {
        try {
            String Usuario = username.getText().toString().trim();
            String Password = password.getText().toString().trim();
            String Confirm = confirm.getText().toString().trim();
            if (Usuario.equals("") && Password.equals("") && Confirm.equals("")) {
                utils.showToast("Debe ingresar todos los datos del formulario del registro");
                return false;
            } else if (Usuario.equals("")) {
                utils.showToast("Debe ingresar el usuario");
                return false;
            } else if (Password.equals("")) {
                utils.showToast("Debe ingresar la contraseña");
                return false;
            } else if (Confirm.equals("")) {
                utils.showToast("Debe ingresar la contraseña de confirmación");
                return false;
            } else if (!Confirm.equals(Password)) {
                utils.showToast("La contraseña de confirmación no coincide, favor de verificar");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean ReadUser(String usuario, String Password) {
        try {
            String info = sharedData.getStringData(MY_JSON);
            if (info.equals("")) {
                if (!sharedData.SaveData(MY_JSON, JsonUser(usuario, Password), false, 0, 0)) {
                    return false;
                }
            } else {
                if (!gson.toJson(info).equals("[]")) {
                    jArray = new JSONArray(info);
                    boolean encontrado = false;
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        if (json_data.getString("Usuario").equals(usuario)) {
                            utils.showToast("El usuario " + usuario + " ya existe");
                            encontrado = true;
                        }
                    }
                    if (!encontrado) {
                        if (!sharedData.SaveData(MY_JSON, JsonUser(usuario, Password), false, 0, 0)) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private void ClearData() {
        username.setText("");
        password.setText("");
        confirm.setText("");
    }
}
