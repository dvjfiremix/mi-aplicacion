package com.merlino.miaplicacion.Home;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.merlino.miaplicacion.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeView extends AppCompatActivity {

    @BindView(R.id.bottomBar)
    BottomNavigationView bottoms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


}
