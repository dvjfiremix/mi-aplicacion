package com.merlino.miaplicacion.Home.Fragments.AddFragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.merlino.miaplicacion.FragmentBase;
import com.merlino.miaplicacion.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import Libraries.SharedData;
import Libraries.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static Libraries.Globals.JSON_FILES;
import static Libraries.Globals.getUSUARIO;
import static android.Manifest.permission.CAMERA;
import static android.app.Activity.RESULT_OK;

public class AddData extends FragmentBase implements View.OnClickListener {

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    private static final int PERMISSION_REQUEST_CODE = 200;
    @BindView(R.id.Imagen)
    CircleImageView imagen;
    @BindView(R.id.titulo)
    EditText titulo;
    @BindView(R.id.descripcion)
    EditText descripcion;
    @BindView(R.id.votar)
    RatingBar votar;
    @BindView(R.id.agregar)
    Button agregar;
    JSONArray ArraysItems = new JSONArray();
    Gson gson = new Gson();
    private HomeLoad homeLoad;
    private SharedData sharedData;
    private Utils utils;
    private boolean LoadImagen = false;
    private String image_uri = "";
    private String miFoto = "";

    public AddData() {
    }

    public AddData(@Nullable HomeLoad homeLoad) {
        this.homeLoad = homeLoad;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.add_design, container, false);
        ButterKnife.bind(this, view);
        sharedData = SharedData.getInstance(getActivity());
        utils = Utils.getInstance(getActivity());
        utils.CreateFolderInternalApp();
        agregar.setOnClickListener(this);
        imagen.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == RESULT_OK) {
                    Uri imagens = utils.getImageFromPath(image_uri);
                    utils.LoadImage(imagens.toString(), imagen);
                    image_uri = imagens.toString();
                    LoadImagen = true;
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.agregar) {
            if (Validate()) {
                try {
                    ReadUser();
                    homeLoad.LoadShow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (id == R.id.Imagen) {
            if (!checkPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermission();
            } else {
                LoadFromCamera();
            }
        }
    }

    private void LoadFromCamera() {
        if (!miFoto.equals("")) {
            utils.clearImagesFromPath(miFoto);
        }
        image_uri = System.currentTimeMillis() + ".jpg";
        miFoto = image_uri;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, utils.getImageFromPath(image_uri));
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public boolean Validate() {
        try {
            String Title = titulo.getText().toString().trim();
            String Descripcion = descripcion.getText().toString().trim();
            if (Title.equals("") && Descripcion.equals("")) {
                utils.showToast("Debe añadir el titulo y descripción");
                return false;
            } else if (Title.equals("")) {
                utils.showToast("Debe añadir el titulo");
                return false;
            } else if (Descripcion.equals("")) {
                utils.showToast("Debe añadir la descripción");
                return false;
            } else if (!LoadImagen) {
                utils.showToast("Debe agregar la foto");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean checkPermission() {
        int cameras = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        return cameras == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean Camera = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (Camera) {
                    LoadFromCamera();
                } else {
                    utils.showToast("El permiso de la cámara es requerido");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(CAMERA)) {
                            RequestPermission("La aplicación necesita acceder a la cámara para poder capturar imágenes",
                                    (dialog, which) -> requestPermissions(new String[]{CAMERA},
                                            PERMISSION_REQUEST_CODE));
                        }
                    }
                }
            }
        }
    }

    public void RequestPermission(String msj, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(msj)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show();
    }

    private String JsonData(String titulo, String desc, String MiFoto, String imagen, float stars) {
        String jsons = null;
        try {
            JSONObject ItemsInfo = new JSONObject();
            ItemsInfo.put("Usuario", getUSUARIO());
            ItemsInfo.put("Titulo", titulo);
            ItemsInfo.put("Descripcion", desc);
            ItemsInfo.put("Foto", MiFoto);
            ItemsInfo.put("Imagen", imagen);
            ItemsInfo.put("Stars", stars);
            ArraysItems.put(ItemsInfo);
            jsons = ArraysItems.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsons;
    }

    private void ReadUser() {
        try {
            String Title = titulo.getText().toString().trim();
            String Descripcion = descripcion.getText().toString().trim();
            String info = sharedData.getStringData(JSON_FILES);
            if (info.equals("")) {
                sharedData.SaveData(JSON_FILES, JsonData(Title, Descripcion, miFoto, image_uri, votar.getRating()), false, 0, 0);
            } else {
                if (!gson.toJson(info).equals("[]")) {
                    ArraysItems = new JSONArray(info);
                    sharedData.SaveData(JSON_FILES, JsonData(Title, Descripcion, miFoto, image_uri, votar.getRating()), false, 0, 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface HomeLoad {
        void LoadShow();
    }

}
