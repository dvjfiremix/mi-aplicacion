package com.merlino.miaplicacion.Home.Datas;

import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.merlino.miaplicacion.R;

import Libraries.ItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderModels extends RecyclerView.ViewHolder implements View.OnClickListener {
    @BindView(R.id.Title)
    public TextView titulo;
    @BindView(R.id.Desc)
    public TextView descripcion;
    @BindView(R.id.delete)
    public CoordinatorLayout delete;
    @BindView(R.id.foto)
    public CircleImageView foto;
    @BindView(R.id.counters)
    public RatingBar rating;
    public ItemClickListener itemClickListener;

    public ViewHolderModels(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic) {
        this.itemClickListener = ic;
    }
}
