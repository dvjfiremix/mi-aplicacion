package com.merlino.miaplicacion.Home.Fragments.RemoveFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.merlino.miaplicacion.FragmentBase;
import com.merlino.miaplicacion.Home.Datas.AdapterModels;
import com.merlino.miaplicacion.Home.Datas.ClassModels;
import com.merlino.miaplicacion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Libraries.SharedData;
import Libraries.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

import static Libraries.Globals.JSON_FILES;
import static Libraries.Globals.getUSUARIO;

public class RemoveData extends FragmentBase implements AdapterModels.ActionItems {

    @BindView(R.id.carga)
    RecyclerView carga;
    JSONArray ArraysItems = new JSONArray();
    Gson gson = new Gson();
    @BindView(R.id.titulos)
    LinearLayout titulos;
    private HomeLoad homeLoad;
    private SharedData sharedData;
    private ArrayList<ClassModels> items = new ArrayList<>();
    private AdapterModels adapter;
    private Utils utils;

    public RemoveData() {
    }

    public RemoveData(@Nullable HomeLoad homeLoad) {
        this.homeLoad = homeLoad;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_load, container, false);
        ButterKnife.bind(this, view);
        utils = Utils.getInstance(getActivity());
        sharedData = SharedData.getInstance(getActivity());
        adapter = new AdapterModels(items, this, getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        carga.setHasFixedSize(true);
        carga.setLayoutManager(linearLayoutManager);
        carga.setAdapter(adapter);
        LoadInfo(getUSUARIO());
        return view;
    }

    @Override
    public void Delete(int pos) {
        Borrar(pos);
        utils.showToast("Elemento eliminado correctamente");
        utils.clearImagesFromPath(adapter.getNombreImagen(pos));
        adapter.notifyDataSetChanged();
        Log.d("Nuevo: ", ArraysItems.toString());
        sharedData.SaveData(JSON_FILES, ArraysItems.toString(), false, 0, 0);
        homeLoad.LoadShow();
    }

    private void LoadInfo(String user) {
        try {
            String info = sharedData.getStringData(JSON_FILES);
            items.clear();
            if (info.equals("") || info.equals("[]")) {
                titulos.setVisibility(View.VISIBLE);
            } else {
                boolean encontrado = false;
                if (!gson.toJson(info).equals("[]")) {
                    ArraysItems = new JSONArray(info);
                    for (int i = 0; i < ArraysItems.length(); i++) {
                        JSONObject json_data = ArraysItems.getJSONObject(i);
                        if (json_data.getString("Usuario").equals(user)) {
                            ClassModels infos = new ClassModels();
                            infos.setTitulo(json_data.getString("Titulo"));
                            infos.setDescripcion(json_data.getString("Descripcion"));
                            infos.setImagen(json_data.getString("Imagen"));
                            infos.setRating(json_data.getLong("Stars"));
                            infos.setNombreImagen(json_data.getString("Foto"));
                            infos.setType(1);
                            items.add(infos);
                            encontrado = true;
                        }
                    }

                    if (!encontrado) {
                        titulos.setVisibility(View.VISIBLE);
                    } else {
                        titulos.setVisibility(View.GONE);
                    }
                } else {
                    titulos.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    private void Borrar(int num) {
        try {
            String info = sharedData.getStringData(JSON_FILES);
            if (!gson.toJson(info).equals("[]")) {
                ArraysItems = new JSONArray(info);
                for (int i = 0; i < ArraysItems.length(); i++) {
                    JSONObject json_data = ArraysItems.getJSONObject(i);
                    if (json_data.getString("Usuario").equals(getUSUARIO()) && json_data.getString("Foto").equals(adapter.getNombreImagen(num))) {
                        ArraysItems.remove(i);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    public interface HomeLoad {
        void LoadShow();
    }


}
