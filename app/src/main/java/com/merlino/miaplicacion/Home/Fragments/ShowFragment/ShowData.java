package com.merlino.miaplicacion.Home.Fragments.ShowFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.merlino.miaplicacion.FragmentBase;
import com.merlino.miaplicacion.Home.Datas.AdapterModels;
import com.merlino.miaplicacion.Home.Datas.ClassModels;
import com.merlino.miaplicacion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Libraries.SharedData;
import butterknife.BindView;
import butterknife.ButterKnife;

import static Libraries.Globals.JSON_FILES;
import static Libraries.Globals.getUSUARIO;

public class ShowData extends FragmentBase {
    @BindView(R.id.carga)
    RecyclerView carga;
    @BindView(R.id.titulos)
    LinearLayout titulos;
    JSONArray ArraysItems = new JSONArray();
    private SharedData sharedData;
    private ArrayList<ClassModels> items = new ArrayList<>();
    private AdapterModels adapter;
    private Gson gson = new Gson();

    public ShowData() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_load, container, false);
        ButterKnife.bind(this, view);
        sharedData = SharedData.getInstance(getActivity());
        adapter = new AdapterModels(items, null, getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        carga.setHasFixedSize(true);
        carga.setLayoutManager(linearLayoutManager);
        carga.setAdapter(adapter);
        LoadInfo(getUSUARIO());
        return view;
    }

    private void LoadInfo(String user) {
        try {
            items.clear();
            String info = sharedData.getStringData(JSON_FILES);
            Log.d("Info: ", info);
            if (info.equals("") || info.equals("[]")) {
                titulos.setVisibility(View.VISIBLE);
            } else {
                boolean encontrado = false;
                if (!gson.toJson(info).equals("[]")) {
                    ArraysItems = new JSONArray(info);
                    for (int i = 0; i < ArraysItems.length(); i++) {
                        JSONObject json_data = ArraysItems.getJSONObject(i);
                        if (json_data.getString("Usuario").equals(user)) {
                            ClassModels infos = new ClassModels();
                            infos.setTitulo(json_data.getString("Titulo"));
                            infos.setDescripcion(json_data.getString("Descripcion"));
                            infos.setImagen(json_data.getString("Imagen"));
                            infos.setRating(json_data.getLong("Stars"));
                            items.add(infos);
                            encontrado = true;
                        }
                    }
                    if (!encontrado) {
                        titulos.setVisibility(View.VISIBLE);
                    } else {
                        titulos.setVisibility(View.GONE);
                    }
                } else {
                    titulos.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

}
