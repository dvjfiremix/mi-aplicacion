package com.merlino.miaplicacion.Home;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.merlino.miaplicacion.Home.Fragments.AddFragment.AddData;
import com.merlino.miaplicacion.Home.Fragments.RemoveFragment.RemoveData;
import com.merlino.miaplicacion.Home.Fragments.ShowFragment.ShowData;
import com.merlino.miaplicacion.R;

public class HomeModel extends HomeView implements RemoveData.HomeLoad, AddData.HomeLoad, BottomNavigationView.OnNavigationItemSelectedListener {
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bottoms.setOnNavigationItemSelectedListener(this);
        SelectItem(R.id.option1);
    }

    private void setFragment(int position) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (position) {
                case 0:
                    fragment = new ShowData();
                    fragmentTransaction.replace(R.id.content, fragment);
                    fragmentTransaction.commitAllowingStateLoss();
                    break;
                case 1:
                    fragment = new AddData(this);
                    fragmentTransaction.replace(R.id.content, fragment);
                    fragmentTransaction.commit();
                    break;
                case 2:
                    fragment = new RemoveData(this);
                    fragmentTransaction.replace(R.id.content, fragment);
                    fragmentTransaction.commitAllowingStateLoss();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SelectItem(int id) {
        if (id == R.id.option1) {
            setFragment(0);
        } else if (id == R.id.option2) {
            setFragment(1);
        } else {
            setFragment(2);
        }
    }

    @Override
    public void LoadShow() {
        setFragment(0);
        SelectItem(R.id.option1);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        SelectItem(item.getItemId());
        return false;
    }
}
