package com.merlino.miaplicacion.Home.Datas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.merlino.miaplicacion.R;

import java.util.ArrayList;

import Libraries.Utils;

public class AdapterModels extends RecyclerView.Adapter<ViewHolderModels> {
    private ArrayList<ClassModels> items;
    private ActionItems actions;
    private Utils utils;

    public AdapterModels(ArrayList<ClassModels> Items, ActionItems actionItems, Context context) {
        this.items = Items;
        this.actions = actionItems;
        utils = Utils.getInstance(context);
    }

    @NonNull
    @Override
    public ViewHolderModels onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_design, null);
        return new ViewHolderModels(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderModels holder, int position) {
        final ClassModels datos = items.get(position);
        if (datos.getType() == 0) {
            holder.delete.setVisibility(View.GONE);
        } else {
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(v -> actions.Delete(position));
        }
        holder.rating.setRating(datos.rating);
        holder.titulo.setText(datos.titulo);
        holder.descripcion.setText(datos.descripcion);
        utils.LoadImage(datos.imagen, holder.foto);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public String getNombreImagen(int pos) {
        return items.get(pos).getNombreImagen();
    }

    public interface ActionItems {
        void Delete(int pos);
    }

}
