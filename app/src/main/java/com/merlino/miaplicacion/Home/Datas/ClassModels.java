package com.merlino.miaplicacion.Home.Datas;

public class ClassModels {
    int id;
    String titulo;
    String descripcion;
    String imagen;
    String NombreImagen;
    float rating;
    int type;

    public ClassModels() {
        this.titulo = "Ejemplo";
        this.descripcion = "Descripción";
        this.imagen = "MiImagen";
        this.NombreImagen = "MiFoto";
        this.rating = 0;
        this.type = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNombreImagen() {
        return NombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        NombreImagen = nombreImagen;
    }
}
